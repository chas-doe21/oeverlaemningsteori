# Handover

To handover or take over a codebase is something very common.

People join and leave teams and companies, projects change maintainer, etc.

Let's reflect a bit around it&#x2026;


# Take over a codebase

It could be that you inherit a project from a colleague, it could be that you just joined the team and need to get up-to-speed with it, you'll need to get to know the codebase.

Resist the urge to "rewrite it from scratch" even if at first sight it seems the easiest solution. Often it's not even up to you to decide if you *get to* rewrite it anyway&#x2026;


## Time factor

When taking over a project from someone who's leaving the team/company, time is of the essence. If you're lucky there'll be some overlap between when you start and when the old maintainer leaves.

Make the most of this time, since once the old maintainer leaves it's on you to understand, fix and further develop the codebase/project.


## What to ask for

So what should you ask from the previous maintainer?

First of all, accept the fact that, depending on the size of the codebase, you may not have enough time to get through all of it during the handover period. Try to get an overview of all the moving parts involved, at the very least.


### Documentation

Ask to get access to the project's documentation from day 1.

Comprehensive and up-to-date documentation will help you immensely.

A lack of documentation is a red-flag.


### Tests

Are there tests in place? Unit tests? Integration tests?

How much of the codebase is covered by the tests?

A lack of tests is another red-flag to look out for.

Having tests in place will allow you to focus more on getting to know the code, and less on making sure the code works as expected. Existing unit tests will also come in handy to make sure your new changes to the code didn't break any older part.


### Credentials

Ask for a complete list of credentials used in the project (API keys, servers and services).


### Code walkthrough

If there is enough time, ask the previous maintainer to walk you through the code.

This will give you a first explanation of the whole codebase, and you can ask questions and get examples of the design patterns used in the project.


### Environment setup guidelines

Along with documentation, ask for guidelines on how to setup all the needed environments (i.e. dev/staging/prod), as well as a list of all dependencies.


### Setup a development env

run tests, and try out the code. If anything goes awry, ask the older maintainer for clarification.


## If it's not broken&#x2026;

&#x2026;don't fix it.

You'll very likely will inherit a backlog as well. Make sure to prioritize the right user stories. Don't fix working stuff just because "I would have done it another way".


## Keep documentation up to date

from day 1.

This will help you in the long run.


# Hand over a project

The other side of the medal, you're on your way out and somebody else comes in to take over.


## Time is of the essence&#x2026;

&#x2026;more for the person taking over than for you. You probably want to spend your last couple of weeks on tidying up loose threads, and preparing for your next adventure.

If you have extensive documentation and testing in place, the handover will be a walk in the park. Otherwise, you'll probably spend a lot of your time answering the new maintainer's questions.


## Motivation

May be a bit lacking when handing over a project. It's definitely not as exciting as taking over a new codebase, or starting a new project.

The new maintainer will make up for it with an endless stream of questions, requests, meeting bookings, &#x2026;


## Be honest

You're out of there, anyway.

Be honest with the new maintainer about which part of the code work well, and which part less so. Is any aspect of the project sub-par? Is there anything you haven't had time to fix yet?


## The social part

If working for a big organisation your team probably has to do with other teams. Make sure to introduce the new maintainer to all the mail groups, slack channels and shared documents as needed to insure a painless transition.

Sometimes a few sentences explaining the key people involved in a project go a very long way for the new maintainer, i.e.:

-   X works with ops. He's the person you want to talk to if you have problems with a deployment. Actually, Y is in charge of deployments, but X usually answers more quickly and is more helpful.

-   X is the project owner for this project. She|he usually isn't very enforcing of daily standups, but expects a weekly mail status update on the project. Don't talk to him|her before they've had their coffee.

-   X is the go-to person for all your questions about git inside the organization.

ETC&#x2026;


# LABB

Now it is time to try it all out.

1.  Randomize who takes over whose project
2.  Get going with the handover

Don't forget to take not of how your new project is handed over to you. You'll need to leave a feedback on that.

Don't forget to fork the project you take over!


## Checklist

Here's an example checklist of questions:

-   is there enough documentation?
-   are there any tests in place?
-   is there a backlog?
-   are the user stories in the backlog well-defined or very vague?
-   do the user storeis in the backlog have explicit priorities or do you need to figure out what to work on?
-   how easy is it to get ahold of the old maintainer?
-   did you get any info on how to deploy/use the code?